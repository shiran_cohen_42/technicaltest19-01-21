const FIZZ_BUZZ_CONFIG = {
    FIZZ : {VALUE:3, MESSAGE:'Fizz'},
    BUZZ : {VALUE:5, MESSAGE:'Buzz'}
}

// Display FizzBuzz values between 1 and given 'range'
const FizzBuzz = function(range){
    // Not a number
    if (!range)
        console.log(`Range for FizzBuzz is missing or zero (input: ${range})`);
    else if (isNaN(range))
        console.log(`Range limit has to be a number (input: ${range})`);
    // Range too small
    else if (range < 1)
        console.log(`Range limit is too small to play (input: ${range})`);
    // Range valid
    else {
        console.log(`Playing fizzbuzz with range: ${range}`);
        // Present message for every number in range
        for (let index = 1; index <= range; index++){
            let numberMessage = '';

            // Fizz 
            if (index % FIZZ_BUZZ_CONFIG.FIZZ.VALUE == 0)
                numberMessage += FIZZ_BUZZ_CONFIG.FIZZ.MESSAGE;
            // Buzz
            if (index % FIZZ_BUZZ_CONFIG.BUZZ.VALUE == 0)
                numberMessage += FIZZ_BUZZ_CONFIG.BUZZ.MESSAGE;
        
            // Complete message (FizzBuzz message, or number)
            console.log(numberMessage || index.toString());
        }
    }
}

module.exports =FizzBuzz;