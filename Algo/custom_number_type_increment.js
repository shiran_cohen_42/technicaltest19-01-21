// Sequential digits poiting the next digit (example cell index '5' has value '6')
const SEQUENTIAL_DIGITS_IN_NUMBER = ['1','2','3','4','5','6','7','8','9','0'];

// Increment number array by 1
// Return carry or error message (true if 1, false if 0, error if any)
const incrementCellRecursive = function(array, index){
    // Done but have carry of 1
    if (index < 0)
        return true;   
    // Increase next digit
    else {
        // Calculate next incremented digit
        let currentDigit = array[index].toString();
        let nextDigit =  SEQUENTIAL_DIGITS_IN_NUMBER[currentDigit];
        
        // Array cell value curropted - cannot increment
        if (!nextDigit){
            let errorMessage = `Currupted data in array. Expecting character digit, encountered: ${currentDigit}`;
            console.error(errorMessage);
            return errorMessage;
        }
        else {
            // Increment current digit by 1
            array[index] = nextDigit;

            // Carry
            if (nextDigit == 0)
                return incrementCellRecursive(array, index - 1);

            // Done
            else
                return false;
        }
    }
}

// Increment number represented by digit array by 1. final carry wil display on "final_carry"
const increment = function(array){
    // Array object curropted - do nothing
    if (!array || ! array.length) {
        let errorMessage = `Error: Expecting array of at least 1 value(s), receivied: ${array.toString()}`;
        console.error(errorMessage);
        return errorMessage;
    }
    else {
        // Make array copy
        var resultArray = JSON.parse(JSON.stringify(array));
        
        // Begin increment and save carry (boolean)
        let operationResult = incrementCellRecursive(resultArray, resultArray.length - 1);        

        // Error in operation 
        if (typeof(operationResult) != 'boolean')
            return 'Error in incrementation';
        // Add carry if any, and return new array
        else {
            let carry = operationResult;
            return (carry ? [1] : [] ).concat(resultArray)
        }
    }
}

module.exports = {
    increment
}