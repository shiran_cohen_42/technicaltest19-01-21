const fizzBuzz =  require('./fizzbuzz');
const fizzBuzzTestValues = [-1,0,'Shiran',{},{val:3},[],[3],'10',11.2,'15,4',20,100];

const testFizzBuzz = function(){
    fizzBuzzTestValues.forEach(value=>{
        console.log('*************************************************');
        console.log('Testing FizzBuzz for input: ' + value.toString());
        fizzBuzz(value);
    });
}

const customIncrement = require('./custom_number_type_increment');
const customIncrementTestValues = [
    [1,2,'j'],
    [1,2,9],
    [1,2,3],
    ['1','2','3'],
    [],
    [9],
    [1,'9'],
    [9,9],
    ['9',3,'9'],
    [1,8,9,9],
    ['s',1,2,9],
    [9,9,9,9]
]

const testCustomIncrement = function(){
    customIncrementTestValues.forEach(array=>{
        console.log('*****************************************************************************');
        console.log('Testing incremention for input: ' + array.toString());
        let resultArray = customIncrement.increment(array);

        // Valid result, print
        if (resultArray && resultArray.length)
            console.log(`Result array: ${resultArray.toString()}`);
        else 
            console.log('No result.');
    });
}

// ALgo tests:
testFizzBuzz();
testCustomIncrement();
