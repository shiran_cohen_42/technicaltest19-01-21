const fs = require('fs');
const fleetSystem = require("../Domain/fleet_system");
const fleet_data_file_path = '../Infra/fleets_data.json';

// Load initial fleet data from database 
let fleetsData = require(fleet_data_file_path)
fleetSystem.setFleets(fleetsData);

function saveFleetData(){
    fs.writeFileSync(fleet_data_file_path, JSON.stringify(fleetSystem.getAllFleets(),null,2));
}

let action = process.argv[2];
let userId, fleetId, plateNumer, lat, lng, alt;

switch (action){
    case "create":
        userId = process.argv[3];
        if (!userId)
            return console.error("No user id given");
        newFleetId = fleetSystem.getNewFleetId(userId);
        console.log(`Created fleet for user ${userId}, fleet id: ${newFleetId}`);
        saveFleetData();
        break;
    case "register-vehicle":
        fleetId = process.argv[3];
        plateNumber = process.argv[4];
        if (!fleetId || !plateNumber)
            return console.error("Missing fleet id or plate number");
        fleetSystem.registerVehicle(fleetId, plateNumber);
        saveFleetData();
        break;
    case "localize-vehicle":
        fleetId = process.argv[3];
        plateNumber = process.argv[4];
        lat = process.argv[5];
        lng = process.argv[6];
        alt = process.argv[7];
        if (!lat || ! lng)
            return console.error(`Missing lattitude or longitude (recieved: ${lat}, ${lng})`);
        let gpsCords = lat + '-' + lng + (alt ? ('-' + alt) : '');
        fleetSystem.parkVehicle(fleetId, plateNumber, gpsCords);
        saveFleetData();
        break;
    default:
        console.error(`Unknown action value: ${action}`);
}