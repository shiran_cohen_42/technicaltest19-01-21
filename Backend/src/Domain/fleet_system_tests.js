const fleetSystem = require("./fleet_system");

// Set random userid
const userId = "TestUser";
const fleetId = fleetSystem.getNewFleetId(userId); // New fleet # 1

// Test cases

// New vehicles 
fleetSystem.registerVehicle(fleetId, "Toyota-yaris");
fleetSystem.registerVehicle(fleetId, "Kia-picanto");
fleetSystem.registerVehicle(fleetId, "Toyota-aygo");

// Duplicate vehicle - Error
fleetSystem.registerVehicle(fleetId, "Toyota-yaris");

// Park vehicles 
fleetSystem.parkVehicle(fleetId, "Toyota-yaris", "victor hugo 87");
fleetSystem.parkVehicle(fleetId, "Toyota-yaris", "victor hugo 34");
fleetSystem.parkVehicle(fleetId, "Toyota-aygo", "grande rue 92");

// Park vehicle in same place - Error
fleetSystem.parkVehicle(fleetId, "Toyota-yaris", "victor hugo 34");

// Park non existing vehicle - Error
fleetSystem.parkVehicle(fleetId, "Fiat-punto", "grande rue 12");

// Adding same vehicle to another fleet
let userId2 = "TestUser2";
let fleetId2 = fleetSystem.getNewFleetId(userId2); // New fleet # 2
fleetSystem.registerVehicle(fleetId2,"Toyota-yaris");

const fs = require('fs');
fs.writeFileSync("./fleets_samle_data.json", JSON.stringify(fleetSystem.getAllFleets(),null,2));