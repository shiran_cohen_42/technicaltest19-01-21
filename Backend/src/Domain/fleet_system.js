var fleets = {};

// Load fleets from externnal data source 
const setFleets = function(fleetsDataObj){
    // Copy given fleets to memory of module
    fleets = JSON.parse(JSON.stringify(fleetsDataObj));
}

// Export fleets 
const getAllFleets = function(){
    // return a copy of the fleets in memory of module
    return JSON.parse(JSON.stringify(fleets));
}

// Register vehicle to fleet
const registerVehicle = function(fleetId, vehicleId){
    // wrong fleetId
    if (!fleets[fleetId])
        return console.error(`fleet '${fleetId}' was not found`);
    // duplicate vehicleId
    if (fleets[fleetId].vehicles[vehicleId]) 
        return console.error(`Vehicle '${vehicleId}' already registered in ${fleets[fleetId].userId}'s fleet`);
    
    // Registering vehicle as required
    let userFleet = fleets[fleetId];
    userFleet.vehicles[vehicleId] = {location:''};
    console.log(`Registered vehicle ${vehicleId} on ${userFleet.userId}'s fleet`);
}

// Park user's vehicle
const parkVehicle = function(fleetId, vehicleId, locationValue){
    // wrong fleetId
    if (!fleets[fleetId])
        return console.error(`fleet '${fleetId}' was not found`);
    
    let userFleet = fleets[fleetId];

    // wrong vehicleId
    if (!userFleet.vehicles || !userFleet.vehicles[vehicleId]) 
        return console.error(`Vehicle '${vehicleId}' doesn't exist for user ${fleets[fleetId].userId}`);

    let userVehicle = userFleet.vehicles[vehicleId];

    // Vehicle already parked there
    if (userVehicle.location === locationValue)
        return console.error(`${fleets[fleetId].userId}'s vehicle '${vehicleId}' is already parked at '${locationValue}'`);
    
    // Parking vehicle as required
    userVehicle.location = locationValue; 
    console.log(`Parked vehicle ${vehicleId} at ${locationValue} (fleet id ${fleetId})`);   
}

// Generate new fleet and unique id
const getNewFleetId = function(userId){
    // Create new fleet for user 
    let newFleetId = "fleet" + Date.now();    
    fleets[newFleetId] = {
        "userId":userId, 
        vehicles:{}
    };

    return newFleetId; 
};

const getVehicleLocation = function(fleetId, vehiceId){
    // wrong fleetId
    if (!fleets[fleetId])
        return console.error(`fleet '${fleetId}' was not found`);
    
    let userFleet = fleets[fleetId];

    // wrong vehicleId
    if (!userFleet.vehicles || !userFleet.vehicles[vehicleId]) 
        return console.error(`Vehicle '${vehicleId}' doesn't exist for user ${fleets[fleetId].userId}`);

    let userVehicle = userFleet.vehicles[vehicleId];

    // Return vehicle location
    return `${userFleet.userId}'s vehicle ${vehicleId} is located at: ${userVehicle.location}`;
}

module.exports = {
    setFleets,
    getAllFleets,
    getNewFleetId,
    registerVehicle,
    parkVehicle,
    getVehicleLocation
}
