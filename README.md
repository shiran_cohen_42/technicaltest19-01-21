# README #

Technical test for inextenso digital
Shiran Cohen
Date: 19/01/21

Algo Test
=========

#############
### Code: ###
#############
Algo/FizzBuzz.js
Algo/custom_number_type_increment.js

##############
### Tests: ###
##############
in Algo folder:
node .\test_algo.js

Backend Test
============

#############
### Code: ###
#############
Level 1 -
Backend/src/Domaine/fleet_system.js  

Level 2 -
Backend/src/App/fleet.js
Backend/src/Infra/fleet_data.json	(used as local database)

##############
### Tests: ###
##############
Level 1 -
in Backend/src/ folder:
node .\fleet_system_tests.js

Level 2 - 
in directory Backend/src/App:
node .\fleet.js %action% %userId or fleetId% [lat] [lng] [alt]


notes:
------
* The database is saved in local json file and not external DB.